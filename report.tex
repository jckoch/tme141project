% Created 2020-01-03 Fri 14:56
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[margin=0.7in]{geometry}
\usepackage[parfill]{parskip}
\usepackage{listings}
\usepackage{color}
\author{James Koch}
\date{\today}
\title{Eigenfrequencies and modes found from snapback loading of a helical spring\\\medskip
\large TME141 (Q2 2019) - Structural Dynamics Assignment}
\hypersetup{
 pdfauthor={James Koch},
 pdftitle={Eigenfrequencies and modes found from snapback loading of a helical spring},
 pdfkeywords={structural dynamics, helical spring, eigenfrequencies, eigenmodes},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\definecolor{mygreen}{RGB}{28,172,0}   % color values Red & Green & Blue
\definecolor{mylilas}{RGB}{170,55,241}
\lstset{language=Matlab,%
  %basicstyle=\color{red},
  breaklines=true,                                  %
  morekeywords={matlab2tikz},                       %
  keywordstyle=\color{blue},                        % 
  morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
  identifierstyle=\color{black},                    %
  stringstyle=\color{mylilas},                      
  commentstyle=\color{mygreen},                     %
  showstringspaces=false,                           % without this there will be a symbol in the places where there is a space
  numbers=left,                                     %
  numberstyle={\tiny \color{black}},                % size of the numbers
  numbersep=9pt,                                    % this defines how far the numbers are from the text
  emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise  
}

\begin{titlepage}
  \begin{center}
    \Large{TME141 (Q2 2019) - Structural Dynamics Assignment}\\
    \vspace{2cm}
    \Huge{Eigenfrequencies and modes found from snapback loading of a helical spring}\\
    \vspace{2cm}
    \large{James C Koch}\\
    \small{cjames@student.chalmers.se}\\
    \vspace{2cm}
    \begin{center}
      \includegraphics[scale=0.7]{./figs/problemgeometry.png}
    \end{center}
    \vfill
  \end{center}
\end{titlepage}

\newpage

\section*{Introduction}
\label{sec:orgd9aa6aa}
In this report the transient response of a helical spring which is subjected to the instantaneous release from a statically loaded state is studied.
This report will describe the background and methods needed to model the helical spring using a finite element (FE) model and compare the model predictions with experimental data provided \cite{assigntext_2019}.
The report will be divided into three sections each corresponding to one part of the methodology used to model the helical spring using finite elements.
The first part of the methodology will focus on computing the eigenvalues and eigenmodes of the FE model and compare with the experimentally derived test values.
The second and third parts of the methodology will focus on simulating the snap-back response and calibrating the model, respectively.

\section*{Background: Finite element modelling and methodology}
\label{sec:org0682bd6}
\subsection*{Eigenvalue problem and Frequency Response}
\label{sec:org05554d8}
To first motivate the methodology and FE approach used here to determine the transient response of the helical spring, a short background starting from the structural dynamics equation is presented in Equation \ref{eq-SDE}.

\begin{align} \label{eq-SDE}
  \mathbf{M} \ddot{\vec{u}}(t) + \mathbf{C} \dot{\vec{u}}(t) + \mathbf{K} \vec{u}(t) = \vec{f}(t)
\end{align}

By considering the free vibration problem, where \(\vec{f}(t) = \vec{0}\), the so-called eigenvalue problem can be derived (Eq. \ref{eq-EVP}) where damping is neglected.

\begin{align} 
  \mathbf{M} \ddot{\vec{u}}(t) + \mathbf{K} \vec{u}(t) &= \vec{0} \nonumber\\
  \text{Assuming } \vec{u}(t) = \hat{\vec{u}} e^{i\omega t} \text{ leading to } \nonumber\\
  [\mathbf{K} - \omega^2 \mathbf{M}] \hat{\vec{u}} e^{i\omega t} &= \vec{0} \nonumber\\
  [\mathbf{K} - \omega^2 \mathbf{M}] \hat{\vec{u}} &= \vec{0} \label{eq-EVP}
\end{align}

As in any eigenvalue problem (Eq. \ref{eq-EVP}), the system of equations has non-trivial solutions \(\hat{\vec{u}} \neq \vec{0}\) given that \(|\mathbf{K} - \omega^2 \mathbf{M}| = 0\).
This expression \(|\mathbf{K} - \omega^2 \mathbf{M}| = 0\) is known as the \emph{characteristic polynomial} in terms of \(\omega^2\).
The roots to the \emph{characteristic polynomial} are known as the eigenvalues which represent the system's resonant frequencies.
Furthermore, it is the non-trivial vectors \(\hat{\vec{u}}_k\) in Equation \ref{eq-EVP} which are defined as the eigenvectors, \(\vec{\phi}_k\), associated to the eigenvalues, \(\omega_k^2\).

Furthermore, it is in the frequency response domain, where the dynamic stiffness and dynamic flexibility (i.e. the transfer function), are defined.
The frequency response domain can be defined by introducing the following two relationships in Equation \ref{eq-FRD}.

\begin{align} \label{eq-FRD}
  \vec{u} &= Re(\hat{\vec{u}} e^{i\omega t}) \rightarrow
  \ddot{\vec{u}} = Re(-\omega^2 \hat{\vec{u}} e^{i\omega t}) \nonumber\\
  \vec{f} &= Re(\hat{\vec{f}} e^{i\omega t})    
\end{align}

Using the relationships shown in Equation \ref{eq-FRDeq} above, the structural dynamics equation \ref{eq-SDE} can be written as follows in Equation \ref{eq-FRDeq}.
It can be shown below that the transfer function, which maps a force applied anywhere on the structure to the response anywhere else (i.e. displacement, velocity, or acceleration), is defined as the dynamic flexibility matrix, \(\mathbf{H}(\omega) = \mathbf{Z}^{-1}(\omega) = [\mathbf{K} + i \omega \mathbf{C} - \omega^2 \mathbf{M}]^{-1}\) (when damping is included as in the results presented in this report).

\begin{align} \label{eq-FRDeq}
  - \omega^2 \mathbf{M} \hat{\vec{u}} + i \omega \mathbf{C} \hat{\vec{u}} + \mathbf{K} \hat{\vec{u}} &= \hat{\vec{f}} \nonumber\\
  [\mathbf{K} + i \omega \mathbf{C} - \omega^2 \mathbf{M}] \hat{\vec{u}} &= \hat{\vec{f}} \nonumber\\
  \mathbf{Z}(\omega) \hat{\vec{u}} = \hat{\vec{f}}
\end{align}

\subsection*{Snap-back simulation}
\label{sec:org0494b39}
Next to provide some background of the methodology used to simulate the snap-back velocity response of the helical spring, it is necessary to consider the state space representation of the governing equation of motion (i.e. the structural dynamics equation) shown in Equation \ref{eq-SDE}.
The general principle of the state space representation is to transform the 2nd order governing differential equation (Equation \ref{eq-SDE}) to a system of 1st order differential equations.
The main idea is to introduce two new variables, \(\vec{x}_1 = \vec{u}\) and \(\vec{x}_2 = \dot{\vec{u}} = \dot{\vec{x}}_1\), for the displacement \(\vec{u}\) and velocity \(\dot{\vec{u}}\), respectively.
The state space model is represented in Equation \ref{eq-SSmodel}.

\begin{align} \label{eq-SSmodel}
  \dot{\vec{x}} &= \mathbf{A} \vec{x} + \mathbf{B} \vec{s} \nonumber\\
  \vec{r} &= \mathbf{C} \vec{x} + \mathbf{D} \vec{s}\\
\end{align}

The state variable, \(\vec{x}\), is equal to \((\vec{x}_1; \vec{x}_2)\) while the derivative of the state variable is \(\dot{\vec{x}} = (\dot{\vec{x}}_1; \ddot{\vec{x}}_2)\).
The system matrices \(\mathbf{A}\), \(\mathbf{B}\), \(\mathbf{C}\), and \(\mathbf{D}\) are defined in Equations \ref{eq-systemmatrices}.

\begin{align} \label{eq-systemmatrices}
  \mathbf{A} &= \begin{bmatrix}
    \mathbf{0} & \mathbf{I}\\
    -\mathbf{M}^{-1} \mathbf{K} & -\mathbf{M}^{-1} \mathbf{C}
  \end{bmatrix} \nonumber\\
  \mathbf{B} &= \begin{bmatrix}
    \mathbf{0}\\
    \mathbf{M}^{-1} \vec{P}_s
  \end{bmatrix} \nonumber\\
  \mathbf{C} &= \begin{bmatrix}
    \mathbf{0} & \mathbf{I}
  \end{bmatrix} \nonumber\\
  \mathbf{D} &= \mathbf{0}
\end{align}

Furthermore, the system matrices need to be defined with certain sizes based on the number of degrees of freedom, \(n_s\), where the stimuli act and the number of degrees of freedom, \(n_r\), where the responses are desired.
For the specific problem at hand, the stimulus acts only at 1 degree of freedom (i.e. \(n_s = 1\)) while the velocities are desired at 3 degrees of freedom (i.e. \(n_r = 3\).
Since the stimulus acts only at 1 degree of freedom, the matrix, \(\vec{P}_s\), acts as a selection matrix to choose the desired columns of \(\mathbf{M}^{-1}\) corresponding to the degrees of freedom where the force acts.
Therefore, the size of the system matrices to get the desired output should be \(\mathbf{A}_{(2n)x(2x)}\), \(\mathbf{B}_{(2n)x(n_s)}\), \(\mathbf{C}_{(n_r)x(2n)}\), and \(\mathbf{D}_{3x1}\).

\subsection*{Model calibration}
\label{sec:org6d514f3}
Thirdly and finally, the FE model is calibrated according to the experimental data.
The difference between the first 6 flexible eigenfrequencies determined experimentally and calculated from the FE model is computed and minimized in a non-linear least squares optimization solver \cite{lsqnonlin_2019}.
To iterate to a better fitted FE model, 5 parameters of the FE model are chosen to be varied in the chosen optimizer.

\section*{Results}
\label{sec:org5ab47dc}
\subsection*{Eigenvalue problem and frequency response functions}
\label{sec:orge4da01e}
Initially the eigenvalue problem (EVP) is computed for the initial FE model.
The eigenvalues and eigenvectors which result from the solution of this EVP are presented in Table \ref{tab-EVP}.
It should be noted that the eigenvectors are not explicitly stated in Table \ref{tab-EVP} but rather a correlation value, MAC-value, between experimental and FE model eigenvectors for the first 6 flexible eigenmodes is calculated.

\begin{table}[htbp]
\caption{\label{tab:org6af39ed}
Eigenfrequencies, modal damping, eigenvector correlation of the 6 lowest frequency elastic eigenmodes. \label{tab-EVP}}
\centering
\begin{tabular}{rrrrrrr}
\hline
Mode & Test \(f\) & FEM \(f\) & Diff (\%) & Damping & MAC & Calibrated \(f\)\\
\hline
7 & 43.4 & 43.54 & 0.32258065 & 0.090 & 0.9619 & 42.9\\
8 & 44.9 & 44.39 & 1.1358575 & 0.030 & 0.9995 & 43.8\\
9 & 46.6 & 47.57 & 2.0815451 & 0.030 & 0.9886 & 46.7\\
10 & 53.2 & 54.88 & 3.1578947 & 0.164 & 0.9437 & 53.8\\
11 & 85.3 & 87.55 & 2.6377491 & 0.080 & 0.9940 & 86.0\\
12 & 93.8 & 94.63 & 0.88486141 & 0.020 & 0.9594 & 93.1\\
\hline
\end{tabular}
\end{table}

The difference between experimental and FE model eigenfrequencies are all less than 4\% for the first 6 flexible eigenmodes which is a good starting point for a numerical FE model.
The eigenvectors are also well correlated using the MAC method as all values are greater than 0.95 with mode 10 being the exception which indicates a good level of fit.

Furthermore, the results of the frequency response functions (aka transfer functions) in the frequency domain are shown from point of load application to the position of the three accelerometers positioned on the helical spring during the experiment in Figure \ref{fig-FRF}.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.6\textwidth]{./figs/subtaska.eps}
\caption{\label{fig:orge5fe737}
Frequency response functions with respect to acceleration at the sensor locations. \label{fig-FRF}}
\end{figure}

As shown in Figure \ref{fig-FRF}, the comparison of experimental and FE model frequency response functions is similar in that all of the peaks (representing eigenfrequencies) are represented but with varying frequency magnitudes.
This seems to suggest that some errors may be introduced from processing the experimental data to identify the experimental frequency response functions but also that the FE model could be further calibrated to resemble the experiment better.

\subsection*{Snap-back simulation}
\label{sec:org35f6d91}
The second part of the methodology is to simulate the time domain solution to the vibrating helical spring.
As mentioned in the \hyperref[sec:org0494b39]{previous section} discussing the background theoretical framework, a state space model implementation is performed numerically using the FE model to simulate the velocity response, shown in Figure \ref{fig-snapbacksimulation}, of the helical spring for the first 0.05s after release from the initial statically loaded state.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.6\textwidth]{./figs/subtaskb.eps}
\caption{\label{fig:org72490e7}
Snap-back simulation of the first 0.05s after the removal of the hanging weight. \label{fig-snapbacksimulation}}
\end{figure}

It should be noted that the velocity response is compared to the experimental velocity measured during the test and a increasing deviation is found between experiment and FE model as time progresses.
This is by no ways a unique problem as most often in time domain solutions the errors in a numerical FE model simulation tend to compound on each other even with more complex time stepping numerical integration routines.

\subsection*{Model calibration}
\label{sec:org68ccb55}
The final and third step conducted was to calibrate the FE model to agree better with the experimental data. 
To do this, five parameters were varied in a non-linear least squares optimizer which included the pitch, \(p\), spring diameter, \(D\), coil diameter, \(d\), Young's modulus, \(E\), and density of the spring material, \(\rho\).
The initial and final values of these five parameters are \(p = 0.083m \rightarrow 0.083m\), \(D = 0.134m \rightarrow 0.135m\), \(d = 0.012m \rightarrow 0.012m\), \(E = 210GPa \rightarrow 210GPa\), and \(\rho = 7850 \frac{kg}{m^3} \rightarrow 7850 \frac{kg}{m^3}\) which is a change of \(1\), \(1.0097\), \(1\), \(1\), and \(1\), respectively.
Table \ref{tab-calibration} shows the results of the iterations performed using the \texttt{lsqnonlin} routine in MATLAB to calibrate the FE model according the given 5 parameters above.

\begin{table}[htbp]
\caption{\label{tab:org66b1db4}
Results from the non-linear least squares optimization routine. \label{tab-calibration}}
\centering
\begin{tabular}{rrrrr}
\hline
Iteration & Func-count & f(x) & Norm of step & First-order optimality\\
\hline
0 & 6 & 9.88 &  & 814\\
1 & 12 & 2.87 & 0.0086 & 226\\
2 & 18 & 2.84 & 0.00024 & 96.9\\
3 & 24 & 2.82 & 0.00026 & 3.47\\
4 & 30 & 2.82 & 0.00071 & 84.9\\
5 & 36 & 2.82 & 0.00012 & 4.12\\
6 & 42 & 2.82 & 0.00017 & 4.12\\
7 & 48 & 2.82 & 4.48e-05 & 4.12\\
8 & 54 & 2.82 & 1.12e-05 & 4.12\\
9 & 60 & 2.82 & 2.80e-06 & 4.12\\
10 & 66 & 2.82 & 7.00e-07 & 4.12\\
\hline
\end{tabular}
\end{table}

Furthermore, the calibration input into \texttt{lsqnonlin} in MATLAB is given in terms of normalized values and calculating the actual parameter values only to update the FE code itself to obtain the new solution to the EVP (see code below).

\begin{lstlisting}
  % normalize the parameters
  xb = [p, D, d, E, rho];
  x0 = [1, 1, 1, 1, 1];

  % optimization: least squares non-linear solver
  options = optimoptions('lsqnonlin','Display','iter');
  x = lsqnonlin(@(x)objfunc(x, xb, lb_exp), x0, [], [], options);

  % the objective function
  function delta = objfunc(x, xb, lb_exp)
      % define the not normalized paramters
      x = x.*xb;

      % update the FE spring model
      UpdateSpringModel(x(1), x(2), x(3), x(4), x(5));
      sys_update = structdyn('data/SpringUpdated.aat'); %update path!!!
      K_new = sys_update.Kfull;
      M_new = sys_update.Mfull;

      % solve the EVP problem
      opt.disp = 0; opt.isreal=1;
      lb = eigs(K_new, (M_new+M_new')/2, 12, 'sm', opt);
      lb = sqrt(abs(lb(7:12)))/(2*pi);

      % calculate the objective function output
      delta = lb_exp - lb;
  end   
\end{lstlisting}

\section*{Concluding remarks}
\label{sec:org509ddcf}
To conclude, the results of developing a FE model of a helical spring to compare with experimental results was presented in this report.
The initial configuration of input parameters of the FE model provides a decent fit with the experimental data but by performing a non-linear least squares optimization algorithm on the FE code a better fit is possible.
Therefore, it can be said that model calibration is an important step to ensure adequate accuracy of a FE model especially for structural dynamics problems as opposed to static structural FE analysis problems.
In addition, when simulating the response of a structural dynamics problem, it should be kept in mind that the numerical integration scheme is important as the numerical errors introduced will cause the numerical simulated response to drift further away from experimental results as time progresses.
This can be compensated for by using appropriate time stepping numerical algorithms but also with a calibrated FE model.

\section*{References}
\label{sec:org56852b0}
\bibliography{references/references}
\bibliographystyle{unsrt}
\section*{Appendices}
\label{sec:orgabe633e}
\newpage
\subsection*{Appendix A: Part A}
\label{sec:org6088d9d}

\lstinputlisting[language=Matlab]{./src/assign_partA.m}

\newpage
\subsection*{Appendix B: Part B}
\label{sec:orgc5547cf}

\lstinputlisting[language=Matlab]{./src/assign_partB.m}

\newpage
\subsection*{Appendix C: Part C}
\label{sec:org3817ad9}

\lstinputlisting[language=Matlab]{./src/assign_partC.m}
\lstinputlisting[language=Matlab]{./src/objfunc.m}
\end{document}