function C = modaldamp(K,M,z,zind,z0,opt)
%MODALDAMP   Establish the viscous damping matrix from given modal dampings
%Inputs: K,M  - Stiffness and mass matrices
%        z    - Relative modal dampings, either as scalar (meaning same
%               damping for all modes) or as vector
%        zind - (optional) index vector for damping values of z
%               z(zind(I)) specifies I:th mode�s damping value
%        z0   - Damping of modes not specified in zind (default: 0.01);
%        opt  - See code
%Output: C    - Viscous damping matrix
%Call:   C = modaldamp(K,M,z[,zind,z0,opt])

%Written:   2012-09-04, Thomas Abrahamsson
%Modified:  2012-09-27, Addded default damping /TA
%Corrected: 2013-03-30, Corrected for damping spec. /TA
%Modified:  2013-12-22, Eliminated try-catch construct causing problem /TA
%Modified:  2014-01-25, Added eigensolution options. /TA
%Modified:  2019-12-03, Cleaned up syntax to enhance readability. /JCK

%Reference: Craig & Curdila, Fundamentals of Structural Dynamics, p305

%% Initiate and test
if nargin<6
    opt = [];
end
if nargin<4
    zind = [];
end
zdef = 0.01;
N = size(K,1);
if length(z) == 1
    z = z*ones(N,1);
end
if isempty(zind)
    zind = 1:length(z);
end
if ~isempty(zind)
    if length(z) ~= length(zind)
        error('z and zind need to be of same length');
    end
end

%% Solve eigenvalue problem and sort
try
    if strcmpi(opt.eig,'eig')
        [V,D] = eig(full(K),full(M));
    elseif strcmpi(opt.eig,'eigs')
        diagM = diag(M);smallM = opt.smr*norm(diagM);
        M = M+smallM*speye(size(M));
        OPTS.maxit = 100;
        [V,D] = eigs(K+opt.mu*M,M,opt.eign,opt.eigp,OPTS);
        D = diag(diag(D)-opt.mu);
    else
        error('Does not recognize eigensolution type')
    end          
catch
    [V,D] = eig(full(K),full(M));
end    
[Ds,inds] = sort(diag(D));Ds(Ds<0) = 0;
V = V(:,inds);

%% Assume that specified damping is for flexible body modes only
try 
    NRBf = sum(Ds<(opt.RBf*2*pi)^2);
catch
    NRBf = 0;
end
zind = zind+NRBf;

%% Prepare damping value vector
% if length(z) == 1
%   z = z*ones(N,1);
% else
zs = z;
%   try z = z0*ones(N,1); catch z = zdef*ones(N,1);end
if exist('z0','var')
    [iz,jz] = size(z0);
else
    iz = 0;jz = 0;
end
if (iz == 1 && jz == 1)
    z = z0*ones(N,1);
else
    z  =  zdef*ones(N,1);
end
  
z(zind) = zs;

%% Compute damping matrix
Md  =  diag(V'*M*V);
iMd  =  sparse(diag(1./Md));
z  =  z(1:length(Md));
Cd  =  sparse(diag(2*z.*sqrt(Ds).*Md));
C  =  (M*V)*iMd*Cd*iMd*(V.'*M);