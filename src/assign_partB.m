%% Part b: Simulate the snap-back (voluntary)
% define state space model
% the dynamic equation
A = [zeros(size(sys.Kfull, 1)), eye(size(sys.Kfull, 1));
     -sys.Mfull^(-1)*sys.Kfull, -sys.Mfull^(-1)*V];
Ps = zeros(size(sys.Kfull, 1), 1);
Ps(pnts(1), 1) = 1;
B = [zeros(size(sys.Kfull, 1), 1);
     sys.Mfull^(-1)*Ps];

% the response equation for velocity
C = [zeros(size(sys.Kfull, 1)) eye(size(sys.Kfull, 1))];
C = C(pnts', :);
D = zeros(size(sys.Kfull, 1));
D = D(pnts, pnts(1));

% state space model
SS = ss(A, B, C, D);

% define input history
dt = 0.02/1000; %s
tend = 0.05;    %s
m = 0.38;       %kg
g = 9.81;       %m/s^2
t = t02(1:2500);
s = ones(1, length(t02))*m*g;

% simulate (create plot) for velocity
tic;
Y = lsim(SS, s(1:2500), t);
toc;

% plotting velocity response
figure(2);
subplot(311);
plot(t, Y(:, 1));
hold on;
plot(t, v_1(1:2500));
xlabel('Time (s)');
ylabel('v_1 (m/s)');
title('Velocity Response');
subplot(312);
plot(t, Y(:, 2));
hold on;
plot(t, v_2(1:2500));
xlabel('Time (s)');
ylabel('v_2 (m/s)');
subplot(313);
plot(t, Y(:, 3));
hold on;
plot(t, v_3(1:2500));
xlabel('Time (s)');
ylabel('v_3 (m/s)');
saveas(2, './figs/subtaskb', 'epsc')