function UpdateSpringModel(p,D,d,E,rho)
if nargin<5, error('Five input arguments required'); end
if (p<0.01 || p>0.2), error('Pitch is either too small or too big'); end
if (D<0.05 || D>0.3), error('Main diameter is either too small or too big'); end
if (d<0.006 || d>0.020), error('Wire diameter is either too small or too big'); end
if (E<100e9 || E>300e9), error('Young''s modulus is either too small or too big'); end
if (rho<6000 || rho>9000), error('Density is either too small or too big'); end

% Nominal values:
%p=0.083; D=0.134; d=0.012; E=210.e9; rho=7850;


fin=fopen('data/Spring_justassemble.aat','r');
fout=fopen('data/SpringUpdated.aat','w');

joints=false;
mp=false; mpno=0;
while true
  txt=fgetl(fin);
  if txt==-1, break, end
  if strcmpi(txt(1),'*'), joints=false; mp=false; end
  try
    if strcmpi(txt(1:17),'member properties')
    mp=true;
    nums=str2num(txt);
    A=pi*d^2/4; Iy=pi*d^4/64; Iz=Iy; Kv=2*Iy;
    fprintf(fout,'MEMBER PROPERTIES %e %e %e %e\n',[A Iy Iz Kv]);
    mpno=1;
  end
  catch
  end
  
  if joints
      nums=str2num(txt);
      x=(D/0.134)*nums(2);
      y=(D/0.134)*nums(3);
      z=(p/0.083)*nums(4);
  end
  if joints
      fprintf(fout,'%i %e %e %e\n',[nums(1) x y z]);
  elseif mp
    if mpno==1
       mpno=2;
    elseif mpno==2   
       A=pi*d^2/4; Iy=pi*d^4/64;
       G=E/2.6; m=rho*A; rp=sqrt(2*Iy/A);  
       fprintf(fout,'%e %e %e %e\n',[E G m rp]); 
       mpno=3;
    elseif mpno==3 || mpno==4
       fprintf(fout,[txt '\n']);
       mpno=mpno+1;;
    end
  else
      fprintf(fout,[txt '\n']);
  end
  
  try
    if strcmpi(txt(1:5),'joint')
    joints=true;
    end
  catch
  end
  

  
end    

fclose(fin);
fclose(fout);