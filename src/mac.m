function MAC=mac(Z1,Z2,M)
%MAC Computes the MAC-matrix using the mode matrices Z1 and Z2 or the
% weighted MAC using a positive definite symmetrical weighting matrix
%Inputs:   Z1     - First modal matrix containing modes as column vectors
%          Z2     - Second modal matrix containing modes as column vectors
%          M      - Optional weighting matrix. Default = I
%Output:   MAC    - MAC-matrix
%Call:     MAC=mac(Z1,Z2,M)
%See also: macold

% Copyleft: Thomas Abrahamsson, Chalmers University of Technology, Sweden
% Written:  Oct   21, 2015

%%                                                       Initiate and check
[mZ1,nZ1]=size(Z1);[mZ2,nZ2]=size(Z2);
if mZ1~=mZ2,error('Row dimension of matrices need to be same');end

%%                                                       Apply weight
if nargin<3
  W=speye(mZ1);
else  
  [U,S,V]=svd(M);
  if min(diag(S))<eps,
    error('Weighting matrix needs to be positive definite');
  end
  W=U*sqrt(S)*V';
end  
Z1=W*Z1;Z2=W*Z2;

%%                                                       Standard MAC
MAC=(abs(Z2'*Z1).^2)./(sum(conj(Z2).*Z2)'*sum(conj(Z1).*Z1));
