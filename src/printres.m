function printres
%PRINTTRES: Print results to file

% Copyleft: Thomas Abrahamsson, Chalmers University of Technology, Sweden
% Written: 2005-12-30 /TA
% Modified 2006-04-15 to get rid of most M-Lint warnings /TA

% -------------------------------------------------------------------------
%                                                                   Globals
%                                                                   -------
global Topo Prop Case Harmp Eigp omeig ModalMass Runtime DBname
global SD

[Name,Path]=uiputfile('*.lst','LINVIB Save results file');
fid=fopen([Path Name],'wt');

% -------------------------------------------------------------------------
%                                                               Get handles
%                                                               -----------
Struct_disp_flag=get(SD.Prnctrl1,'Value');
Struct_velo_flag=get(SD.Prnctrl2,'Value');
Struct_acce_flag=get(SD.Prnctrl3,'Value');
Reacti_load_flag=get(SD.Prnctrl4,'Value');
Member_disp_flag=get(SD.Prnctrl5,'Value');
Member_forc_flag=get(SD.Prnctrl6,'Value');


fprintf(fid,' *************************************************************\n');
fprintf(fid,' *                                                           *\n');
fprintf(fid,' *   STRUCTDYN (v2006.0)                                     *\n');
fprintf(fid,' *                                                           *\n');
fprintf(fid,' *   MATLAB program for linear structural vibration          *\n');
fprintf(fid,' *                                                           *\n');
fprintf(fid,' *   By Thomas Abrahamsson                                   *\n');
fprintf(fid,' *   Applied Mechanics, Chalmers University of Technology    *\n');
fprintf(fid,' *                                                           *\n');
fprintf(fid,[' *   Computation below was made ' datestr(Runtime) '         *\n']);
fprintf(fid,' *                                                           *\n');
fprintf(fid,' *************************************************************\n');


fprintf(fid,' \n');
fprintf(fid,'OUTPUT DATA\n');
fprintf(fid,'***********\n');

if size(Eigp,2)>0
    fprintf(fid,' \n');
    fprintf(fid,'FREE HARMONIC VIBRATION\n');
    fprintf(fid,'***********************\n');
    fprintf(fid,' \n');
end    
for I=1:size(Eigp,2)
  fprintf(fid,['EIGENFREQUENCY AND EIGENMODE NUMBER ' int2str(I) '\n']);
  fprintf(fid,'-------------------------------------------------------------------------\n');
  fprintf(fid,['OMEGA=' sprintf('%0.2f',omeig(I)) '       F=OMEGA/2PI=' sprintf('%0.2f',omeig(I)/2/pi) '      T=1/F=' sprintf('%0.2f',1/(omeig(I)/2/pi)) '  N=60F=' sprintf('%0.2f',60*omeig(I)/2/pi) '\n']);
  fprintf(fid,'EIGENMODE IS NORMALIZED SUCH THAT SUM OF SQUARES\n');
  fprintf(fid,'OF STRUCTURE DISPLACEMENT COMPONENTS EQUALS UNITY\n');
  if Struct_disp_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=Eigp((-5:0)+6*II,I)';
    end   
    prettyprint(11,IDmat,Datamat,fid)
  end
  if Struct_velo_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=i*omeig(I)*Eigp((-5:0)+6*II,I)';
    end   
    prettyprint(12,IDmat,Datamat,fid)
  end
  if Struct_acce_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=-omeig(I)^2*Eigp((-5:0)+6*II,I)';
    end   
    prettyprint(13,IDmat,Datamat,fid)
  end
  if Reacti_load_flag
    IDmat=[];Datamat=[];
    [p,R]=HarmLoadCase(Topo,Prop,Case,omeig(I),Eigp(:,I));
    NJoints=length(Topo.Grid.id);
    for II=1:NJoints;
      ind=find(Topo.Grid.fix(II,:)==1); 
      if ~isempty(ind),IDmat=[IDmat;II];Datamat=[Datamat;R((-5:0)+II*6,1).'];end
    end   
    prettyprint(14,IDmat,Datamat,fid);
  end    
  if Member_disp_flag
    IDmat=[];Datamat=[];
    eval('NoBMemb=size(Topo.Beam.id,1);','NoBMemb=0;')
    for II=1:NoBMemb;
      [Ee,dofs,El,T]=beamdynstiff(omeig(I),II,Topo,Prop,1);
      pincode=Topo.Beam.pincode(II,:);Free=find(pincode==0);Fixed=find(pincode==1);
      pl=T*Eigp(dofs,I);pb=pl(Fixed,1);
      pa=-El(Free,Free)\El(Free,Fixed)*pb;
      pl([Free(:);Fixed(:)],1)=[pa;pb];
      IDmat=[IDmat;[Topo.Beam.id(II,1) Topo.Beam.grids(II,:)]];Datamat=[Datamat;pl(:).'];
    end   
    prettyprint(15,IDmat,Datamat,fid);
  end    
  if Member_forc_flag
    IDmat=[];Datamat=[];
    eval('NoBMemb=size(Topo.Beam.id,1);','NoBMemb=0;')
    for II=1:NoBMemb;
      [Ee,dofs,El,T]=beamdynstiff(omeig(I),II,Topo,Prop,1);
      pincode=Topo.Beam.pincode(II,:);Free=find(pincode==0);Fixed=find(pincode==1);
      pl=T*Eigp(dofs,I);pb=pl(Fixed,1);
      pa=-El(Free,Free)\El(Free,Fixed)*pb;
      pl([Free(:);Fixed(:)],1)=[pa;pb];
      Pl=El*pl;      
      IDmat=[IDmat;[Topo.Beam.id(II,1) Topo.Beam.grids(II,:)]];Datamat=[Datamat;Pl(:).'];
    end   
    prettyprint(16,IDmat,Datamat,fid);
  end    

end


if size(Harmp,2)>0 && length(Case.Hload.evalf)<1
    fprintf(fid,' \n');
    fprintf(fid,'FORCED HARMONIC VIBRATION\n');
    fprintf(fid,'*************************\n');
    fprintf(fid,' ');
    
 for I=1:size(Harmp,2)
  fprintf(fid,['HARMONIC LOADING CASE ' int2str(I) ' OMEGA ' sprintf('%0.2f',Case.Hload.data(I,2)) '\n']);
  fprintf(fid,'-----------------------------------------------------\n');
  if Struct_disp_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=Harmp((-5:0)+6*II,I)';
    end   
    prettyprint(1,IDmat,Datamat,fid)
  end
  if Struct_velo_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=i*Case.Hload.data(I,2)*Harmp((-5:0)+6*II,I)';
    end   
    prettyprint(2,IDmat,Datamat,fid)
  end
  if Struct_acce_flag
    Datamat=[];
    IDmat=Topo.Grid.id(:,1);
    for II=1:size(IDmat,1);
      Datamat(II,1:6)=-Case.Hload.data(I,2)^2*Harmp((-5:0)+6*II,I)';
    end   
    prettyprint(3,IDmat,Datamat,fid)
  end
  if Reacti_load_flag
    IDmat=[];Datamat=[];om=Case.Hload.data(I,2);
    [p,R]=HarmLoadCase(Topo,Prop,Case,om,Harmp(:,I));
    NJoints=length(Topo.Grid.id);
    for II=1:NJoints;
      ind=find(Topo.Grid.fix(II,:)==1); 
      if ~isempty(ind),IDmat=[IDmat;II];Datamat=[Datamat;R((-5:0)+II*6,1).'];end
    end   
    prettyprint(4,IDmat,Datamat,fid);
  end    
  if Member_disp_flag
    IDmat=[];Datamat=[];
    eval('NoBMemb=size(Topo.Beam.id,1);','NoBMemb=0;')
    for II=1:NoBMemb;
      [Ee,dofs,El,T]=beamdynstiff(Case.Hload.data(I,2),II,Topo,Prop,1);
      pincode=Topo.Beam.pincode(II,:);Free=find(pincode==0);Fixed=find(pincode==1);
      pl=T*Harmp(dofs,I);pb=pl(Fixed,1);
      pa=-El(Free,Free)\El(Free,Fixed)*pb;
      pl([Free(:);Fixed(:)],1)=[pa;pb];
      IDmat=[IDmat;[Topo.Beam.id(II,1) Topo.Beam.grids(II,:)]];Datamat=[Datamat;pl(:).'];
    end   
    prettyprint(5,IDmat,Datamat,fid);
  end    
  if Member_forc_flag
    IDmat=[];Datamat=[];
    eval('NoBMemb=size(Topo.Beam.id,1);','NoBMemb=0;')
    for II=1:NoBMemb;
      [Ee,dofs,El,T]=beamdynstiff(Case.Hload.data(I,2),II,Topo,Prop,1);
      pincode=Topo.Beam.pincode(II,:);Free=find(pincode==0);Fixed=find(pincode==1);
      pl=T*Harmp(dofs,I);pb=pl(Fixed,1);
      pa=-El(Free,Free)\El(Free,Fixed)*pb;
      pl([Free(:);Fixed(:)],1)=[pa;pb];
      Pl=El*pl;      
      IDmat=[IDmat;[Topo.Beam.id(II,1) Topo.Beam.grids(II,:)]];Datamat=[Datamat;Pl(:).'];      
    end   
    prettyprint(6,IDmat,Datamat,fid);
  end    
    
 end
end

fclose(fid);