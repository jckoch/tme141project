function delta = objfunc(x, xb, lb_exp)
    % define the not normalized paramters
    x = x.*xb;
    
    % update the FE spring model
    UpdateSpringModel(x(1), x(2), x(3), x(4), x(5));
    sys_update = structdyn('data/SpringUpdated.aat'); %update path!!!
    K_new = sys_update.Kfull;
    M_new = sys_update.Mfull;
    
    % solve the EVP problem
    opt.disp = 0; opt.isreal=1;
    lb = eigs(K_new, (M_new+M_new')/2, 12, 'sm', opt);
    lb = sqrt(abs(lb(7:12)))/(2*pi);
    
    % calculate the objective function output
    delta = lb_exp - lb;
end    