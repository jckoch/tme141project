close all;
clear all; clc;

addpath('/home/jkoch/MATLAB Add-Ons/Apps/StructDyn');
addpath('/home/jkoch/MATLAB Add-Ons/Apps/StructDyn/StructDyn/Doc');
addpath('/home/jkoch/MATLAB Add-Ons/Apps/StructDyn/StructDyn/Verify_ex');
addpath('/home/jkoch/MATLAB Add-Ons/Apps/StructDyn/StructDyn');
addpath('/home/jkoch/MATLAB Add-Ons/Apps/StructDyn/dynamic');

%% Part 1a: Eigenvalues and Eigenvectors (compulsory)
% define eigenvalues initially from structdyn *.lst file
lb_exp = [43.4 44.9 46.6 53.2 85.3 93.8]';
lb_fem = [43.54 44.39 47.57 54.88 87.55 94.63]';

% define experimental eigenvector elements at sensing locations
ZE = [1      -0.0606 -0.9;
      -0.834 0.173   1;
      -0.934 0.202   1;
      -0.514 0.278   1;
      1     -0.885   0.754;
      0.322 -0.495   1];
% define the FEM eigenvector elements at sensing locations  
ZA = [-4728 1262 3442;
      -7039 1235 8572;
      -15301 888 16292;
      -1838 603 2101;
      13074 -12283 8463;
      6538 -6856 11110]*10^-6;

% eigenvector correlation matrix
MAC = mac(ZE', ZA');
MAC = diag(MAC);

% run structdyn to get stiffness and mass matrices (full)
sys = structdyn('data/Spring_justassemble.aat');

% define viscous damping matrix
z = 0.05/100; % estimate using 0.05% equivalent modal damping???
V = modaldamp(sys.Kfull, sys.Mfull, z);

% define dofs of interest (where are the sensors?)
pnts = [663 303 63];

%% Part 1b: Frequency Response Functions (compulsory)
% load the experimental test data
load('data/SpringTestData.mat');

% compute omega from f
omega = f*2*pi;

% define dynamic stiffness matrix
tic;
FRF_fem = zeros(3, 1, length(f));
for j = 1:length(f) 
    Z = (sys.Kfull + 1i*omega(j)*V - omega(j)^2*sys.Mfull); %*10^-6;
    H = -omega(j)^2*inv(Z); %transfer function for acceleration
    FRF_fem(:, :, j) = H(pnts(1), pnts')';
end
toc;

% plotting
figure(1);
subplot(311);
semilogy(f, abs(squeeze(FRF_fem(3, 1, :))));
hold on;
semilogy(f, abs(squeeze(FRF(3, 1, :))));
xlabel('f (Hz)');
title('f_1 to a_1');
subplot(312);
semilogy(f, abs(squeeze(FRF_fem(2, 1, :))));
hold on;
semilogy(f, abs(squeeze(FRF(2, 1, :))));
xlabel('f (Hz)');
title('f_1 to a_2');
subplot(313);
semilogy(f, abs(squeeze(FRF_fem(1, 1, :))));
hold on;
semilogy(f, abs(squeeze(FRF(1, 1, :))));
xlabel('f (Hz)');
title('f_1 to a_3');
saveas(1, './figs/subtaska', 'epsc');
