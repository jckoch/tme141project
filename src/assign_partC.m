%% Part c: Calibrate the model (voluntary)
% define new input geometry data for helical spring
p = 0.083;  %m
D = 0.134;  %m
d = 0.012;  %m
E = 210e9;  %Pa
rho = 7850; %kg/m^3

% normalize the parameters
xb = [p, D, d, E, rho];
x0 = [1, 1, 1, 1, 1];

% optimization: least squares non-linear solver
options = optimoptions('lsqnonlin','Display','iter');
tic;
x = lsqnonlin(@(x)objfunc(x, xb, lb_exp), x0, [], [], options);
toc;

% recompute real parameters
xfinal = x.*xb;

% compute final FE model
UpdateSpringModel(xfinal(1), xfinal(2), xfinal(3), xfinal(4), xfinal(5));
sys_final = structdyn('data/SpringUpdated.aat'); %update this path
                                                 %to whereever your 
                                                 %.aat file is
K_final = sys_final.Kfull;
M_final = sys_final.Mfull;

% solve the EVP problem
opt.disp = 0; opt.isreal=1;
lb_final = eigs(K_final, (M_final+M_final')/2, 12, 'sm', opt);
lb_final = sqrt(abs(lb_final(7:12)))/(2*pi);

% print final eigenvalues from FE model
fprintf('%.1f\n%.1f\n%.1f\n%.1f\n%.1f\n%.1f\n', lb_final);